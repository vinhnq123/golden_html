$(()=>{
    $(document).click(function (event) {
        let clickover = $(event.target);
        console.log(clickover)
        let _opened = $(".navbar-collapse").hasClass("navbar-collapse show");
        if (_opened === true && !clickover.hasClass("navbar-toggle")) {
            $("button.navbar-toggle").click();
        }
    });

    let swiper = new Swiper(".swiper-box", {
        slidesPerView: "auto",
        spaceBetween: 15,
    });

    let swiperBrand2 = new Swiper(".swiper-brand2", {
        slidesPerView: "auto",
        spaceBetween: 15,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });

    let swiperDiscover = new Swiper(".swiper-discover", {
        slidesPerView: "auto",
        spaceBetween: 15,
        pagination: {
            el: '.swiper-pagination',
        },
    });

    if ($(window).width() >= 768){
        let swiperAdv = new Swiper(".swiper-adv", {
            slidesPerView: "auto",
            effect :"coverflow",
            centeredSlides:true,
            grabCursor:true,
            initialSlide:2,
            coverflowEffect:{
                rotate:0,
                stretch:100,
                depth:50,
                modifier:1,
                slideShadows:false
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });
    }else{
        let swiperAdv = new Swiper(".swiper-adv", {
            slidesPerView: "auto",
            spaceBetween: 10,
            pagination: {
                el: '.swiper-pagination',
            },
        });
    }


    if ($(window).width() < 991) {
        let swiper = new Swiper(".swiper-block", {
            slidesPerView: "auto",
            spaceBetween: 15,
        });
    }

    let swiperTeam = new Swiper(".swiper-teamwork", {
        slidesPerView: "auto",
        spaceBetween: 15,
        pagination: {
            el: '.swiper-pagination',
        },
    });
})